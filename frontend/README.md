# A Boilerplate for Dockerizing a Gatsby Site

Clone the repo and use the following make commands to launch the Gatsby development or production environments.

```
# launch the development env
make dev

# launch the production build
make prod
```