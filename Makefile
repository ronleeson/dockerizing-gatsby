build:
	docker-compose build

prod:
	docker-compose up -d

dev:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

down:
	docker-compose down

restart:
	docker-compose stop && docker-compose start

restart-dev:
	docker-compose down && docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d

restart-frontend:
	docker-compose stop frontend && docker-compose start frontend

restart-server:
	docker-compose stop server && docker-compose start server

