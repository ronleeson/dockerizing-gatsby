# To boot up this dockerized Gatsby site:
* open a terminal window and cd to the frontend Gatsby site
* using npm install all the node modules on your local machine
* open another terminal window and run Docker Compose commands
   * to run Gatsby in its development environment run make dev
   * to run Gatsby in production mode run make prod

You can access the Gatsby site at http://localhost:8000

```
cd frontend
# install node modules on your local machine
npm install

# cd to root of project
cd ..
# to run Gatsby in development mode
make dev

# to run and build Gatsby in production mode
make prod
```